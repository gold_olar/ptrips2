const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Now the notification Schema
const NotificationSchema = new Schema({
    title:{
        type: String,
        required: true
    },
    details:{
        type: String,
        required: true
    }
});


const NotificationsModel = mongoose.model('notifications', NotificationSchema);
module.exports = NotificationsModel;