const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const hbs = require('hbs');
const mongoose = require('mongoose');
const session = require('express-session');
const bodyParser = require('body-parser');
const db = require('./config/database');
require('dotenv').config();
const verifyToken = require('./controllers/verifyToken')

//Was having issues with som req handlers...aggg
const signinController = require('./controllers/signinController');
const addController = require('./controllers/addController');


const indexRouter = require('./routes/index');
const usersRouter = require('./routes/profile');
const signupRouter = require('./routes/signup');
const signinRouter = require('./routes/signin');
const dashboardRouter = require('./routes/dashboard');
const passwordRouter = require('./routes/changepassword');
const addRouter = require('./routes/add');
const notificationsRouter = require('./routes/notifications');
const signoutRouter = require('./routes/signout');
const editRouter = require('./routes/edit');
const policyRouter = require('./routes/policy');
const editsessionRouter = require('./routes/editsession');
const changepasswordRouter = ('./routes/userchangepassword')
const deletesessionRouter = require('./routes/deletesession');



const app = express()

//Bodyparser
app.use(bodyParser.urlencoded({
  extended: false
}));

//To Connect to Database
mongoose.connect(db.mongoURI, {
  useNewUrlParser: true,
})
  .then(() => console.log('Connected to DB'))
  .catch(err => console.log(err));


//Express session middleware
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));


//To register Partials
hbs.registerPartials(__dirname + '/views/partials');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');


//signinHandler
app.post('/user/signin', signinController.signinForm);
//addHandler
app.post('/add/session', addController.addSession);









app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/dashboard', dashboardRouter);
app.use('/profile', usersRouter);
app.use('/signup', signupRouter);
app.use('/signin', signinRouter);
app.use('/add', addRouter);
app.use('/notifications', notificationsRouter);
app.use('/signout', signoutRouter);
app.use('/edit', editRouter);
app.use('/changepassword', passwordRouter);
app.use('/policy', policyRouter);
app.use('/editsession', editsessionRouter);
app.use('/deletesession', deletesessionRouter);



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;


