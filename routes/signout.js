const express = require('express');
const router = express.Router();
const secret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');
const verifyToken = require('../controllers/verifyToken');



router.get('/', verifyToken.verifyToken, function (req, res) {
    const header = req.cookies.auth;
     const emptytoken = ""
    res.cookie('auth', header);
    jwt.verify(header, secret, (err, authData) => {
        if (err) {
            res.render('error', {
                message: "You have already signed out."
            });
        } else {
            res.cookie('auth', emptytoken);
            console.log('Signed Out');
            res.redirect('/signin');
             }
       });
});




module.exports = router;
