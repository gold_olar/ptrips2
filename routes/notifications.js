const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const secret = process.env.JWT_SECRET;
const Notifications = require('../models/Notifications');
const verifyToken = require('../controllers/verifyToken');




router.get('/', verifyToken.verifyToken ,(req, res, next)=>{
    const header = req.cookies.auth;
    res.cookie('auth', header);
    jwt.verify(header, secret, (err, authData) => {
        if (err) {
            res.render('error', {
                message: "Please Sign in Again"
            });
        } else {
            Notifications.find({})
            .sort({ date: 'desc' })
            .then(notifications =>{
                res.render('notifications',{
                    // not_title: notifications.title,
                    // details: notifications.details,
                    notifications:notifications,
                    title: "Private Trips || Notifications",
                    name: authData.user,
                    currentYear: new Date().getFullYear(), 
                            
                 });

            });
        }
  
    });
});


module.exports = router;
