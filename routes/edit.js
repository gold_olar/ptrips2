const express = require('express');
const router = express.Router();
const secret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');
const verifyToken = require('../controllers/verifyToken');
const Session = require('../models/Session');


router.get('/:id', verifyToken.verifyToken, function (req, res) {
    const header = req.cookies.auth;
    res.cookie('auth', header);
    jwt.verify(header, secret, (err, authData) => {
        if (err) {
            res.render('error', {
                message: "Please Sign in Again"
            });
        } else {
            Session.findOne({
                _id: req.params.id
            })
            .then(session =>{
                res.render('edit',{
                    title: session.title,
                    details: session.details,
                    id:session.id,
                    name: authData.user
                });
            });
           
             }
       });
});

module.exports = router;



