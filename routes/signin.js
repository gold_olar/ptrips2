const express = require('express');
const router = express.Router();



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('signin', { 
    title: 'Private Trips || Signin',
    currentYear: new Date().getFullYear() 
  });
});



module.exports = router;
