const express = require('express');
const router = express.Router();
const  signupControler = require('../controllers/signupController');



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('signup', { 
    title: 'Private Trips || Signup',
    currentYear: new Date().getFullYear() 
  });
});

router.post('/', signupControler.signupForm);


module.exports = router;
