const express = require('express');
const router = express.Router();
const secret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');
const verifyToken = require('../controllers/verifyToken');
const Session = require('../models/Session');


router.get('/', verifyToken.verifyToken, function (req, res) {
    const header = req.cookies.auth;
    res.cookie('auth', header);
    jwt.verify(header, secret, (err, authData) => {
        if (err) {
            res.render('error', {
                message: "Please Sign in Again"
            });
        } else {
            Session.find({owner:authData.user})
                .sort({ date: 'desc' })
                .then(sessions => {
                    res.render('dashboard', {
                        sessions: sessions,
                        title: 'Private trips || Dashboard',
                        name: authData.user,
                        currentYear: new Date().getFullYear()
                    });
                });
             }
       });
});




module.exports = router;
