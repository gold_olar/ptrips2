const express = require('express');
const router = express.Router();
const secret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');
const verifyToken = require('../controllers/verifyToken');


/* GET home page. */
router.get('/', verifyToken.verifyToken, function (req, res, next) {
  const header = req.cookies.auth;
  jwt.verify(header, secret, (err, authData) => {
    if (!err) {
      res.render('add', {
        title: 'Private Trips || Add ',
        currentYear: new Date().getFullYear(),
        name: authData.user
      });
    } else {
      res.render('error', {
        message: "Please Sign in Again"
      });
    }
  })

});



module.exports = router;
