const express = require('express');
const router = express.Router();
const  profileController = require('../controllers/profileController');
const verifyToken = require('../controllers/verifyToken');

/* GET users listing. */
router.get('/', verifyToken.verifyToken, profileController.profileGetter);


module.exports = router;
