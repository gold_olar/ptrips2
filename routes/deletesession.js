const express= require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const secret = process.env.JWT_SECRET;
const Session = require('../models/Session');

router.get('/:id', function (req, res){
    const header = req.cookies.auth;
    res.cookie('auth', header);
    jwt.verify(header, secret, (err, authData) => {
        if (err) {
            res.render('error', {
                message: "Please Sign in Again"
            });
        } else {
            Session.deleteOne({_id: req.params.id})
            .then(session=>{
                res.redirect('/dashboard');
            })

        }
   
    });
});



module.exports = router;


