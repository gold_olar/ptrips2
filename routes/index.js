const express = require('express');
const router = express.Router();
const  contactControler = require('../controllers/contactController');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    title: 'Private Trips',
    currentYear: new Date().getFullYear() 
  });
});

//To Handle Contact Form.
router.post('/', contactControler.contactForm);

module.exports = router;
