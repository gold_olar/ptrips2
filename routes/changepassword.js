const express = require('express');
const router = express.Router();
const secret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');
const verifyToken= require('../controllers/verifyToken');

router.get('/', verifyToken.verifyToken ,(req, res)=>{
    const header = req.cookies.auth;
    res.cookie('auth', header);
    jwt.verify(header, secret, (err, authData) => {
        if (err) {
            res.render('error', {
                message: "Please Sign in"
            });
        } else {
            res.render('changepassword',{
                name: authData.user
            });
        }
    }); 
});


module.exports = router;