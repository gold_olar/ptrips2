const User = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
require('dotenv').config()
const secret = process.env.JWT_SECRET;


const signinForm = (req, res, next) => {
	let errors = []
	User.findOne({
		username: req.body.username
	}).then(user => {
		if (user) {
			bcrypt.compare(req.body.password, user.password, (err, isMatch) => {
				if (isMatch) {
					//JWT dey here
					jwt.sign({ user: user.username }, secret,{expiresIn :"2h"}, (err, token)=>{
						res.cookie('auth', token);
						res.redirect('/dashboard');
					});					
				} else {
					errors.push({ text: "Wrong Password." })
					if (errors.length > 0) {
						res.render('signin', {
							username: req.body.username,
							password: "",
							errors: errors
						});
					}
				}
			});
		} else {
			errors.push({ text: "Invalid username. Please Sign up or make sure your username is correctly typed." });
			if (errors.length > 0) {
				res.render('signin', {
					username: "",
					password: "",
					errors: errors
				});
			}

		}
	});
}

module.exports = {
	signinForm,
	
}
