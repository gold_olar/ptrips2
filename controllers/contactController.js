const Contact = require('../models/Contact')
const mailer = require ('nodemailer');
require('dotenv').config();

const contactForm = (req, res)=>{
    console.log(111);
    let errors=[];
    if (!req.body.message){
        errors.push({ text: 'You cant send an empty message.' });
    }
    if (errors.length > 0) {
        res.render('index', {
          errors: errors,
          name: req.body.name,
          email: req.body.email,
          news: req.body.news,
          message: req.body.message
        });
      } else{
          try {
            const smtpTransport = mailer.createTransport({
                host: 'smtp.gmail.com',
                //port: 465,
                //secure: true,
                service: 'Gmail',
                auth:{
                    user: process.env.EMAIL,
                    pass: process.env.PASSWORD
                }
            });
            const mail ={
                from:"sam99kupo@gmail.com",
                to: "sam99kupo@gmail.com",
                subject: "YOU HAVE A CONTACT FROM PRIVATE TRIPS",
                html: "<h1> Hi, </h1><br> <h3> My name is " + req.body.name +".</h3><br> <p>" + req.body.message + "</p>"   ,
                text: req.body.message
               }
               smtpTransport.sendMail(mail,(error,response)=>{
                   if (error){
                       console.log(error)
                   }else{
                       console.log('message has been delivered');
                       smtpTransport.close();
                   }
               });
              
          } catch (error) {
              console.log(error);
          } finally{
            console.log('Your message has been sent.');
            if(req.body.news){
                //Here I am saving Customers name to database if they subscribed for news update.
                const newContact = {
                    name: req.body.name,
                    email: req.body.email,
                    }
                    new Contact(newContact)
                    .save()
                    .then(contact => {
                        
                        
                        }) 
            }
          }
          
          res.redirect('/');
        //   req.flash('success_msg', 'Thanks, your message has been sent');
      }

    

}
module.exports={
    contactForm
}