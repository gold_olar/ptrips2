const User = require('../models/User');
const bcrypt= require('bcryptjs');

const signupForm = (req, res)=>{
  let errors = [];
  if (req.body.password != req.body.password2) {
    errors.push({ text: 'Passwords do not match' });
  } if (req.body.password.length < 4) {
    errors.push({ text: 'Password must be at least 4 characters' });
  }if (req.body.username.length < 4) {
    errors.push({ text: 'Username must be at least 4 characters' });
  }if (!(req.body.firstname && req.body.lastname)){
    errors.push({text: 'Please fill in your name'});
  }
  
  if (errors.length > 0 ){
    res.render('signup',{
      errors: errors,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      password2: req.body.password2
    })
  }else{
    User.findOne({email:req.body.email})
    .then(user =>{
      if (user) {
        errors.push({ text: 'Sorry, Email has already been registered.' });
          res.render('signup',{
            errors: errors,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            username: "",
            email: "",
            password: req.body.password,
            password2: req.body.password2
          });
        }else{
          const newUser = new User({
            firstname : req.body.firstname,
            lastname: req.body.lastname,
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
          });
          bcrypt.genSalt(10,(err, salt)=>{
              bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser.save()
                .then(user => {
                  res.redirect('/signin');
                })
                .catch(err => {
                  console.log(err);
                  return;

                });
            });


          })

        }


    })

  }
}



module.exports={
    signupForm
}