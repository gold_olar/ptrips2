const Session = require('../models/Session');


const addSession = (req, res) => {
    let errors = [];
    if (!req.body.title) {
        errors.push({ text: "Title cannot be empty" });
    }
    if (!req.body.details) {
        errors.push({ text: "You have not filled in the details of your diary session" });
    }
    if (errors.length > 0) {
        res.render('add', {
            errors: errors,
            session_title: req.body.title,
            details: req.body.details
        });
    } else {
        const newSession = {
            title: req.body.title,
            details: req.body.details,
            owner: req.body.owner
        }
        new Session(newSession)
            .save()
            .then(session => {
                res.redirect('/dashboard');
            });
    }
}
module.exports = {
    addSession
}


