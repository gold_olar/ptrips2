const User = require('../models/User');
const jwt = require('jsonwebtoken');
const secret = process.env.JWT_SECRET;


const profileGetter = (req, res)=>{
    const header = req.cookies.auth;
    jwt.verify(header, secret, (err, authData)=>{
        if (err){
            res.render('error',{
                message: "Please Sign in Again"
            });
        }else{
            User.findOne({
                username: authData.user
            }).then(user=>{
                if(user){
                  const registered_date = user.date;
                  const display_date = registered_date.toDateString();
                 res.render('profile',{
                     name: authData.user,
                     firstname:user.firstname,
                     lastname: user.lastname,
                     email: user.email,
                     username: user.username,
                     date: display_date
                 });
                };
            });
        }
    })
  
};




module.exports={
    profileGetter
}


 